package com.dreams.chat.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dreams.chat.R;
import com.dreams.chat.interfaces.OnUserGroupItemClick;
import com.dreams.chat.models.LogCall;
import com.dreams.chat.utils.Helper;

import java.util.ArrayList;
import java.util.Locale;

public class LogCallAdapter extends RecyclerView.Adapter<LogCallAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<LogCall> dataList;
    private OnUserGroupItemClick itemClickListener;

    public LogCallAdapter(Context context, ArrayList<LogCall> dataList) {
        this.context = context;
        this.dataList = dataList;

        if (context instanceof OnUserGroupItemClick) {
            this.itemClickListener = (OnUserGroupItemClick) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnUserGroupItemClick");
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_item_log_call, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.setData(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView userImage, aCallLogImg;
        private TextView time, duration, userName;

        MyViewHolder(View itemView) {
            super(itemView);
            userImage = itemView.findViewById(R.id.userImage);
            time = itemView.findViewById(R.id.time);
            duration = itemView.findViewById(R.id.duration);
            userName = itemView.findViewById(R.id.userName);
            aCallLogImg = itemView.findViewById(R.id.img_calllog);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    if (pos != -1) {
                        itemClickListener.OnUserClick(dataList.get(pos).getUser(), pos, userImage);
                    }
                }
            });
        }

        public void setData(LogCall logCall) {
            Glide.with(context).load(logCall.getUser().getImage()).apply(new RequestOptions().placeholder(R.drawable.ic_placeholder)).into(userImage);
            userName.setText(logCall.getUser().getNameToDisplay());
            time.setText(Helper.getDateTime(logCall.getTimeUpdated()));
//            time.setCompoundDrawablesWithIntrinsicBounds(logCall.getStatus().equals("CANCELED") ? R.drawable.ic_call_missed_24dp : logCall.getStatus().equals("DENIED") || logCall.getStatus().equals("IN") ? R.drawable.ic_call_received_24dp : logCall.getStatus().equals("OUT") ? R.drawable.ic_call_made_24dp : 0, 0, 0, 0);
            if (logCall.getStatus().equalsIgnoreCase("CANCELED")) {
                aCallLogImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_call_missed_24dp));
            } else if (logCall.getStatus().equalsIgnoreCase("DENIED")) {
                aCallLogImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_call_missed_24dp));
            } else if (logCall.getStatus().equalsIgnoreCase("IN")) {
                aCallLogImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_in));
            } else if (logCall.getStatus().equalsIgnoreCase("OUT")) {
                aCallLogImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_out));
            }

            duration.setText(formatTimespan(logCall.getTimeDuration()));
        }

        private String formatTimespan(int totalSeconds) {
            long minutes = totalSeconds / 60;
            long seconds = totalSeconds % 60;
            return String.format(Locale.US, "%02d:%02d", minutes, seconds);
        }
    }
}
